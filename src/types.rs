use std::{
    collections::HashMap,
    sync::{Arc, RwLock},
};

use futures::channel::mpsc::UnboundedSender;
use tungstenite::protocol::Message;

// Just an id and a discrete 1D position for now
pub struct Entity {
    pub id: u32,
    pub pos: (i32, i32),
}

impl Entity {
    pub fn to_json(&self) -> String {
        format!(
            "{{\"position\":{{\"x\":{},\"y\":{}}}, \"id\":{}}}",
            self.pos.0, self.pos.1, self.id
        )
    }
}

pub type Connections = Arc<RwLock<HashMap<u32, Tx>>>;
pub type Counter = Arc<RwLock<u32>>;
// pub type Entities = HashMap<u32, Entity>;
pub type Entities = Arc<RwLock<HashMap<u32, Entity>>>;
pub type Tx = UnboundedSender<Message>;
