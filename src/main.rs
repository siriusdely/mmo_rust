extern crate futures;
extern crate tokio;
extern crate tokio_tungstenite;
extern crate tungstenite;

use std::{collections::HashMap, net::SocketAddr, sync::RwLock, time::Duration};

use futures::{channel::mpsc, future, stream::TryStreamExt, StreamExt};
use tokio::net::{TcpListener, TcpStream};
use tokio_tungstenite::tungstenite::Error;
use tungstenite::{protocol::Message, Result};

mod types;

use types::{Connections, Counter, Entities, Entity};

#[tokio::main]
async fn main() {
    let addr = std::env::args()
        .nth(1)
        .unwrap_or_else(|| "127.0.0.1:8080".to_string());

    // Create the event loop and TCP listener we'll accept connections on.
    let try_socket = TcpListener::bind(&addr).await;
    let mut listener = try_socket.expect("Failed to create server");
    println!("Listening on: {}", addr);

    // Hashmap to store a sink value with an id key
    // A sink is used to send data to an open client connection
    let connections = Connections::new(RwLock::new(HashMap::new()));
    // Hashmap of id:entity pairs. This is basically the game state.
    let entities = Entities::new(RwLock::new(HashMap::new()));
    // Used to assign a unique id to each new player
    let counter = Counter::new(RwLock::new(0));

    // Clone references to these states in order to move into the connection_handler closure
    let connections_inner = connections.clone();
    let entities_inner = entities.clone();

    tokio::spawn(interval_loop(connections_inner, entities_inner));

    // Let's spawn the handling of each connection in a separate task.
    while let Ok((stream, addr)) = listener.accept().await {
        // Clone again to move into closure "f"
        let connections_inner = connections.clone();
        let entities_inner = entities.clone();
        let counter_inner = counter.clone();

        tokio::spawn(accept_connection(
            connections_inner,
            entities_inner,
            counter_inner,
            stream,
            addr,
        ));
    }
}

async fn accept_connection(
    connections: Connections,
    entities: Entities,
    counter: Counter,
    stream: TcpStream,
    addr: SocketAddr,
) {
    if let Err(e) = handle_connection(connections, entities, counter, stream, addr).await {
        match e {
            Error::ConnectionClosed | Error::Protocol(_) | Error::Utf8 => (),
            err => println!("Error processing connection: {}", err),
        }
    }
}

// This stream is the game loop
async fn interval_loop(connections: Connections, entities: Entities) {
    // Delay makes the loop run just 10 times a second
    let mut interval = tokio::time::interval(Duration::from_millis(1000));
    let mut interval_future = interval.next();

    loop {
        match interval_future.await {
            _ => {
                let mut conn = connections.write().unwrap();
                let ids = conn.iter().map(|(k, _v)| k.clone()).collect::<Vec<_>>();

                for id in ids.iter() {
                    // Must take ownership of the sink to send on it
                    // The only way to take ownership of a hashmap value is to remove it
                    // And later put it back (line 124)
                    let sink = conn.remove(id).unwrap();

                    /* Meticulously serialize entity vector into json */
                    let entities = entities.read().unwrap();
                    let first = match entities.iter().take(1).next() {
                        Some((_, e)) => e,
                        None => break,
                    };

                    let serial_entities = format!(
                        "[{}]",
                        entities
                            .iter()
                            .skip(1)
                            .map(|(_, e)| e.to_json())
                            .fold(first.to_json(), |acc, s| format!("{},{}", s, acc))
                    );

                    sink.unbounded_send(Message::Text(serial_entities)).unwrap();

                    // Clone for future "f"
                    let connections = connections.clone();
                    let id = id.clone();

                    tokio::spawn(async move {
                        connections.write().unwrap().insert(id.clone(), sink);
                    });
                }

                interval_future = interval.next(); // Wait for next tick.
            }
        }
    }
}

async fn handle_connection(
    connections: Connections,
    entities: Entities,
    counter: Counter,
    stream: TcpStream,
    addr: SocketAddr,
) -> Result<()> {
    println!("Peer address: {}", addr);

    let ws_stream = tokio_tungstenite::accept_async(stream)
        .await
        .expect("Error occurred during the websocket handshake");
    println!("New WebSocket connection: {}", addr);

    {
        // Increment the counter by first locking the RwLock
        let mut c = counter.write().unwrap();
        *c += 1;
    }

    // Assign an id to the new connection and associate with a new entity and the sink
    let id = *counter.read().unwrap();
    let (tx, rx) = mpsc::unbounded::<Message>();
    connections.write().unwrap().insert(id, tx);
    entities
        .write()
        .unwrap()
        .insert(id, Entity { id, pos: (0, 0) }); // Start at position 0

    let c = *counter.read().unwrap();

    let (ws_sender, ws_receiver) = ws_stream.split();
    let receive_from_others = rx.map(Ok).forward(ws_sender);
    let broadcast_incoming = ws_receiver.try_for_each(|msg| {
        if msg.is_text() || msg.is_binary() {
            process_message(c, &msg, entities.clone());

            let peers = connections.write().unwrap();
            // We want to broadcast the message to everyone except ourselves.
            let broadcast_recipients = peers
                .iter()
                .filter(|(peer_addr, _)| peer_addr != &&c)
                .map(|(_, ws_sink)| ws_sink);

            // Echo incoming WebSocket messages and send a message periodically every second.
            for recp in broadcast_recipients {
                recp.unbounded_send(msg.clone()).unwrap();
            }
        } else if msg.is_close() {
            println!("Received a closed message");
        }

        future::ok(())
    });

    futures::pin_mut!(broadcast_incoming, receive_from_others);
    future::select(broadcast_incoming, receive_from_others).await;

    println!("{} disconnected", &addr);

    Ok(())
}

// Update a player's entity state depending on the command they sent
fn process_message(id: u32, msg: &Message, entities: Entities) {
    let txt = msg.to_text().unwrap();
    println!("Received a message {} from id {}", txt, id);

    if txt == "right" {
        entities
            .write()
            .unwrap()
            .entry(id)
            .and_modify(|e| e.pos.0 += 10);
    } else if txt == "left" {
        entities
            .write()
            .unwrap()
            .entry(id)
            .and_modify(|e| e.pos.0 -= 10);
    } else if txt == "down" {
        entities
            .write()
            .unwrap()
            .entry(id)
            .and_modify(|e| e.pos.1 += 10);
    } else if txt == "up" {
        entities
            .write()
            .unwrap()
            .entry(id)
            .and_modify(|e| e.pos.1 -= 10);
    }
}
