use std::{
    collections::HashMap,
    env,
    io::Error as IoError,
    net::SocketAddr,
    sync::{Arc, /*Mutex,*/ RwLock},
    time::Duration,
};

use futures::{
    channel::mpsc::unbounded,
    future::{ok, select, Either},
    pin_mut,
    stream::{TryStream, TryStreamExt},
    SinkExt, StreamExt,
};
use tokio::net::{TcpListener, TcpStream};
use tokio_tungstenite::tungstenite::Error;
use tungstenite::{protocol::Message, Result as TungResult};

pub mod types;

use types::{Entity, PeerMap, Tx};

type Connections = Arc<RwLock<HashMap<u32, Tx>>>;
type Entities = Arc<RwLock<HashMap<u32, Entity>>>;
type Counter = Arc<RwLock<u32>>;

#[tokio::main]
async fn main() -> Result<(), IoError> {
    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| "127.0.0.1:8080".to_string());

    // Create the event loop and TCP listener we'll accept connections on.
    let try_socket = TcpListener::bind(&addr).await;
    let mut listener = try_socket.expect("Failed to create server");
    println!("Listening on: {}", addr);

    // Hashmap to store a sink value with an id key
    // A sink is used to send data to an open client connection
    let connections = Connections::new(RwLock::new(HashMap::new()));
    // Hashmap of id:entity pairs. This is basically the game state.
    let entities = Entities::new(RwLock::new(HashMap::new()));
    // Used to assign a unique id to each new player
    let counter = Counter::new(RwLock::new(0));

    // Clone references to these states in order to move into the connection_handler closure
    let connections_inner = connections.clone();
    let entities_inner = entities.clone();

    // let state = PeerMap::new(Mutex::new(HashMap::new()));

    // Let's spawn the handling of each connection in a separate task.
    while let Ok((stream, addr)) = listener.accept().await {
        // tokio::spawn(_accept_connection(stream));
        // tokio::spawn(_handle_connection(state.clone(), stream, addr));
        // tokio::spawn(__accept_connection(addr, stream));

        // Clone again to move into closure "f"
        let connections_inner = connections_inner.clone();
        let entities_inner = entities_inner.clone();
        let counter_inner = counter.clone();

        tokio::spawn(accept_connection(
            connections_inner,
            entities_inner,
            counter_inner,
            stream,
            addr,
        ));
    }

    Ok(())
}

async fn accept_connection(
    connections: Connections,
    entities: Entities,
    counter: Counter,
    stream: TcpStream,
    addr: SocketAddr,
) {
    if let Err(e) = handle_connection(connections, entities, counter, stream, addr).await {
        match e {
            Error::ConnectionClosed | Error::Protocol(_) | Error::Utf8 => (),
            err => println!("Error processing connection: {}", err),
        }
    }
}

async fn handle_connection(
    connections: Connections,
    entities: Entities,
    counter: Counter,
    stream: TcpStream,
    addr: SocketAddr,
) -> TungResult<()> {
    println!("Peer address: {}", addr);

    let ws_stream = tokio_tungstenite::accept_async(stream)
        .await
        .expect("Error occurred during the websocket handshake");
    println!("New WebSocket connection: {}", addr);

    let (mut ws_sender, mut ws_receiver) = ws_stream.split();
    // Delay makes the loop run just 10 times a second
    let mut interval = tokio::time::interval(Duration::from_millis(100));

    /*
    let (tx, rx) = unbounded::<Message>();
    {
        // Increment the counter by first locking the RwLock
        let mut c = counter.write().unwrap();
        *c += 1;
    }

    // Assign an id to the new connection and associate with a new entity and the sink
    let id = *counter.read().unwrap();
    connections.write().unwrap().insert(id, tx);
    entities
        .write()
        .unwrap()
        .insert(id, types::Entity { id, pos: (0, 0) }); // Start at position 0

    let c = *counter.read().unwrap();
     */

    // Echo incoming WebSocket messages and send a message periodically every second.

    let mut msg_fut = ws_receiver.next();
    let mut tick_fut = interval.next();
    loop {
        match select(msg_fut, tick_fut).await {
            Either::Left((msg, tick_fut_continue)) => {
                match msg {
                    Some(msg) => {
                        let msg = msg?;
                        let c = 1;
                        if msg.is_text() || msg.is_binary() {
                            let txt = msg.to_text().unwrap();
                            // For fun
                            println!("Received a message from {} id {}: {}", addr, c, txt);

                            if txt == "right" {
                                entities
                                    .write()
                                    .unwrap()
                                    .entry(c)
                                    .and_modify(|e| e.pos.0 += 10);
                            } else if txt == "left" {
                                entities
                                    .write()
                                    .unwrap()
                                    .entry(c)
                                    .and_modify(|e| e.pos.0 -= 10);
                            } else if txt == "down" {
                                entities
                                    .write()
                                    .unwrap()
                                    .entry(c)
                                    .and_modify(|e| e.pos.1 += 10);
                            } else if txt == "up" {
                                entities
                                    .write()
                                    .unwrap()
                                    .entry(c)
                                    .and_modify(|e| e.pos.1 -= 10);
                            }
                        } else if msg.is_close() {
                            println!("Received a close message from {} id {}", addr, c);
                        }
                        tick_fut = tick_fut_continue; // Continue waiting for tick.
                        msg_fut = ws_receiver.next(); // Receive next WebSocket message.
                    }
                    None => {
                        // WebSocket stream terminated.
                        println!("Received a None message from {} id {}", addr, 1);
                    }
                };
            }
            Either::Right((_, msg_fut_continue)) => {
                ws_sender.send(Message::Text("tick".to_owned())).await?;
                msg_fut = msg_fut_continue; // Continue receiving the WebSocket message.
                tick_fut = interval.next(); // Wait for next tick.
            }
        }
        /*
        let mut conn = connections_inner.write().unwrap();
        let ids = conn.iter().map(|(k, v)| k.clone()).collect::<Vec<_>>();

        for id in ids.iter() {
            // Must take ownership of the sink to send on it
            // The only way to take ownership of a hashmap value is to remove it
            // And later put it back (line 124)
            let sink = conn.remove(id).unwrap();

            /* Meticulously serialize entity vector into json */
            let entities = entities_inner.read().unwrap();
            let first = match entities.iter().take(1).next() {
                Some((_, e)) => e,
                None => return Ok(Loop::Continue(())),
            };
            let serial_entities = format!(
                "[{}]",
                entities
                    .iter()
                    .skip(1)
                    .map(|(_, e)| e.to_json())
                    .fold(first.to_json(), |acc, s| format!("{},{}", s, acc))
            );

            // Clone for future "f"
            let connections = connections_inner.clone();
            let id = id.clone();

            // This is where the game state is actually sent to the client
            let f = sink
                .send(OwnedMessage::Text(serial_entities))
                .and_then(move |sink| {
                    // Re-insert the entry to the connections map
                    connections.write().unwrap().insert(id.clone(), sink);
                    Ok(())
                })
                .map_err(|_| ());

            executor.spawn(f);
        }
        */
    }

    // pin_mut!(f, g);
    // future::select(f, g).await;

    // println!("{} disconnected", addr);

    Ok(())
}

async fn __accept_connection(peer: SocketAddr, stream: TcpStream) {
    if let Err(e) = __handle_connection(peer, stream).await {
        match e {
            Error::ConnectionClosed | Error::Protocol(_) | Error::Utf8 => (),
            err => println!("Error processing connection: {}", err),
        }
    }
}

async fn __handle_connection(peer: SocketAddr, stream: TcpStream) -> TungResult<()> {
    let ws_stream = tokio_tungstenite::accept_async(stream)
        .await
        .expect("Error occurred during the websocket handshake");
    println!("New WebSocket connection: {}", peer);

    let (mut ws_sender, _receiver) = ws_stream.split();
    let mut interval = tokio::time::interval(Duration::from_millis(100));

    // Echo incoming WebSocket messages
    let mut tick_fut = interval.next();
    loop {
        tick_fut.await;
        ws_sender.send(Message::Text("tick".to_owned())).await?;
        tick_fut = interval.next(); // Wait for next tick.
    }

    // Ok(())
}

async fn _handle_connection(peer_map: PeerMap, raw_stream: TcpStream, addr: SocketAddr) {
    println!("Incoming TCP connection from: {}", addr);

    let ws_stream = tokio_tungstenite::accept_async(raw_stream)
        .await
        .expect("Error occurred the websocket handshake");
    println!("WebSocket connection established: {}", addr);

    // Insert the write part of this peer to the peer map.
    let (tx, rx) = unbounded();
    peer_map.lock().unwrap().insert(addr, tx);

    let (outgoing, incoming) = ws_stream.split();

    let broadcast_incoming = incoming.try_for_each(|msg| {
        println!(
            "Received a message from {}: {}",
            addr,
            msg.to_text().unwrap()
        );

        let peers = peer_map.lock().unwrap();
        // We want to broadcast the message to everyone except ourselves.
        let broadcast_recipients = peers
            .iter()
            .filter(|(peer_addr, _)| peer_addr != &&addr)
            .map(|(_, ws_sink)| ws_sink);

        for recp in broadcast_recipients {
            recp.unbounded_send(msg.clone()).unwrap();
        }

        ok(())
    });

    let receive_from_others = rx.map(Ok).forward(outgoing);

    pin_mut!(broadcast_incoming, receive_from_others);
    select(broadcast_incoming, receive_from_others).await;

    println!("{} disconnected", addr);
}

async fn _accept_connection(stream: TcpStream) {
    let addr = stream
        .peer_addr()
        .expect("connected stream should have a peer address");
    println!("Peer address: {}", addr);

    let ws_stream = tokio_tungstenite::accept_async(stream)
        .await
        .expect("Error occurred during the websocket handshake");
    println!("New WebSocket connection: {}", addr);

    let (write, read) = ws_stream.split();
    read.forward(write)
        .await
        .expect("Failed to forward message");
}
